


public class Job implements Comparable<Job>{

	int ticksLeft;
	int createdAt;
	int endedAt;
	int ticks = 0;
	int jobnum;
	int priority;
	int estimate;
	int consecutiveRuns = 0;
	int timesRun = 0;
	int age = 0;

	public Job(int t, int p, int n, int s){
		ticksLeft = t;
		jobnum = n;
		priority = p;
		estimate = t;
		createdAt = s;
	}
	
	
	public int getRunningTime(){
		return ticksLeft;
	}
	
	public void tick(){
		consecutiveRuns++;
		ticks++;
		ticksLeft--;
	}
	
	public void age(){
		age++;
		
	}
	
	public void ran(){
		timesRun++;
	}
	
	public void end(int t){
		endedAt = t;
	}
	
	public boolean sliced(){
		return consecutiveRuns > (5 - priority);
	}
	
	public boolean expired(){
		return age > estimate;
	}
	
	public int getNum(){
		return jobnum;
	}
	
	public int getEstimate(){
		return estimate;
	}
	
	public int getPriority(){
		return priority;
	}
	
	public String toString(){
		return ( " Job Number: " + jobnum +  
				//" Time Remaining: " + ticksLeft + 
				" Priority: " + priority + " Estimate: " + estimate + " Age: " + age);
		
	}
	
	public String results(){
		return "---Running Time = " + ticks + 
				", Idle Time = " +((endedAt - createdAt) - ticks) + ", Turn-Around Time = " + (endedAt - createdAt) + ", Entered CPU " + timesRun + " time(s).";
	}


	public int compareTo(Job other) {
		return this.priority - other.getPriority();
	}

}
