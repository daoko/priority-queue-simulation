import java.io.*;
import java.util.*;


public class Scheduler {

	
	public static void main(String[] args) throws FileNotFoundException {

		
		ArrayList<Job> readyQueue = new ArrayList<Job>();
		ArrayList<Job> waitingQueue = new ArrayList<Job>();
		ArrayList<Job> finishedJobs = new ArrayList<Job>();
		ArrayList<Event> events = new ArrayList<Event>();
		
		
		//Parse the cmd.txt file for a list of events to simulate
		StringTokenizer splitter;
		BufferedReader br = new BufferedReader(new FileReader(new File("cmd2.txt")));
		String line = null;
		try {
			while((line=br.readLine()) != null){
				
				int time=-1, marker=-1, estimate=-1;
				char jp = '\0';	
				splitter = new StringTokenizer(line);
				
					if(splitter.hasMoreTokens())
						time = Integer.parseInt(splitter.nextToken());
					if(splitter.hasMoreTokens())
						jp = splitter.nextToken().charAt(0);
					if(splitter.hasMoreTokens())
						marker = Integer.parseInt(splitter.nextToken());
					if(splitter.hasMoreTokens())
						estimate = Integer.parseInt(splitter.nextToken());
					 
					//System.out.println(e.toString());

						events.add(new Event(time, jp, marker, estimate));

			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//for(Event c : events)
			//System.out.println(c.toString());
		
		int time = 1;
		int eventIndex = 0;
		int jobNums = 1;
		Job running = null;
		while(true){
			System.out.println("====================");
			System.out.println("Time is: " + time);
			System.out.println("Current jobs in system: " + (readyQueue.size() + waitingQueue.size()));
			Event event = events.get(eventIndex);
			if (event.getTime() == 0){
				System.out.println("Ending Schedule Emulation");
				System.out.println("====================");
				break;
			}

			if(time == event.getTime()){
				//System.out.println("Time matches for event " + event.toString());
				char cmd = event.getCMD();
				switch(cmd){
					//Create a new job
					case 'J':
						System.out.println("Job requested");
						System.out.println("Creating Job #" + jobNums + "  Priority: " 
								+ event.getMarker() + " Estimated Time: " + event.getEstimate());
						readyQueue.add(new Job(event.getEstimate(), event.getMarker(), jobNums, time));
						Collections.sort(readyQueue);
						jobNums++;
						break;
					//Request I/O For the current running job
					case 'W':
						System.out.println("Job I/O requested");
						if(running == null || readyQueue.size() == 0){
							System.out.println("No current job to request I/O on");
							break;
						}
						//Put the current job on the waiting queue
						waitingQueue.add(running);
						System.out.println("Moving current job " + readyQueue.get(0).getNum() + " to waiting queue");
						readyQueue.remove(0);
						
						break;
					case 'R': //Complete I/O for the given job. Move it to the ready queue
						System.out.println("Job I/O complete");
						for(int i=0; i<waitingQueue.size(); i++){
							if(waitingQueue.get(i).getNum() == event.getMarker()){
								System.out.println("I/O Complete for job " + waitingQueue.get(i).getNum() + " Moving to ready queue");
								readyQueue.add(waitingQueue.get(i));
								waitingQueue.remove(i);
							}
								
							}
						break;
					//Complete the current job
					case 'C':
						System.out.println("Completing current job");
						System.out.println(running.toString());
						running.end(time);
						finishedJobs.add(running);
						readyQueue.remove(running);
						break;
					case 'T':
						System.out.println("Termination Requested");
						for(int i=0; i<readyQueue.size(); i++){
							if (readyQueue.get(i).getNum() == event.getMarker()){
								System.out.println("Terminating job " + readyQueue.get(i).getNum());
								readyQueue.get(i).end(time);
								finishedJobs.add(readyQueue.get(i));
								readyQueue.remove(i);
							}
						}
						for(int i=0; i<waitingQueue.size(); i++){
							if (waitingQueue.get(i).getNum() == event.getMarker()){
								System.out.println("Terminating job " + waitingQueue.get(i).getNum());
								finishedJobs.add(waitingQueue.get(i));
								waitingQueue.remove(i);
							}
						}
				}
		
				
				eventIndex++;
			}
			else{
				System.out.println("No external events found");
			}
			
			System.out.println("Processing internal events...");
			int slicer= 0;
			if(running == null){
				running = readyQueue.get(slicer);
				running.ran();
			}
				
			if(readyQueue.size() > 0){
				
				while(running.sliced()){
					if(slicer >= readyQueue.size()){
						System.out.println("No job to switch to");
						break;
					}
					running = readyQueue.get(slicer);
					running.ran();
					System.out.println("Slice expired. Changing current job");
					slicer++;
				}
				System.out.println("Job " + running.getNum() + " running");
				running.tick();
				System.out.println(running.toString());
			}
			
			//Report of all jobs for debugging
			/**
			System.out.println("JOB STATUS REPORT\n******************************");
			System.out.println("R Q");
			for(Job j : readyQueue)
				System.out.println(j.toString());

			System.out.println("W Q");
			for(Job j : waitingQueue)
				System.out.println(j.toString());
			

			System.out.println("******************************");
			*/
			
			
			//Age all jobs in the system
			for(int i=0; i<readyQueue.size(); i++){
				readyQueue.get(i).age();
				if(readyQueue.get(i).expired()){
					System.out.println("Job " + readyQueue.get(i).getNum() + " has expired, removing job");
					readyQueue.remove(i);
				}
			}
			for(int i=0; i<waitingQueue.size(); i++){
				waitingQueue.get(i).age();
				if(waitingQueue.get(i).expired()){
					System.out.println("Job " + waitingQueue.get(i).getNum() + " has expired, removing job");
					waitingQueue.remove(i);
				}
			}

			System.out.println("====================\n");
			time++;	
		}
		
		
		//Print out all jobs still in the system
		/**
		System.out.println("R Q");
		for(Job j : readyQueue)
			System.out.println(j.toString());

		System.out.println("W Q");
		for(Job j : waitingQueue)
			System.out.println(j.toString());
		*/
		System.out.println("Displaying all completed jobs:");
		for(Job j : finishedJobs){
			System.out.println("Job #" + j.getNum() + "::" +j.results());
		}
	}
	
}
