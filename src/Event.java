
public class Event {

	int time;
	char CMD;
	int marker; //Job number or priority, depending on CMD
	int estimate;

	public Event(int t, char c, int m, int e){
		time = t;
		CMD = c;
		marker = m;
		estimate = e;
	}
	
	
	public int getTime(){
		return time;
	}
	
	public char getCMD(){
		return CMD;
	}
	
	public int getMarker(){
		return marker;
	}
	
	public int getEstimate(){
		return estimate;
	}
	
	
	
	public void setTime(int t){
		time = t;
	}
	
	public void setCMD(char c){
		CMD = c;
	}
	
	public void setMarker(int m){
		marker = m;
	}
	
	public void setEstimate(int e){
		estimate = e;
	}
	
	public String toString(){
		return ("Time: " + time + " CMD: " + CMD + " Marker: " + marker + " Est: " + estimate);
		
	}
}
